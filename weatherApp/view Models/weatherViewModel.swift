

import Foundation
class weatherViewModel: ObservableObject {
    
    @Published private var weather : Weather?
    @Published var errorMessage = ""
    
    var tempreture : Double {
        guard let temp = weather?.temp else {
            return 0.0
        }
        return temp 
    }
    
    func fetchWeather(cityName : String) {
        
        guard let city = cityName.escaped() else { return }
        
        weatherService().getWeather(cityName: city) { result in
            switch result {
            case .success(let weatherres):
                DispatchQueue.main.async {
                    self.weather = weatherres
                }
            case .failure(let error):
                print("error \(error.localizedDescription)")
                DispatchQueue.main.async {
                    
                    self.errorMessage = "Unable to find the weather"
                    self.weather = nil
                }
            }
        }
    }
    
    
}
