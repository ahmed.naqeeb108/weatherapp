
//API KEY : afc73a59f0b8c1bf42e5cc947c215d6e

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var weatherVM = weatherViewModel()
    @State private var city = "islamabad"
    
    var body: some View {
        VStack {
            TextField("Search country", text: $city) { _ in
                //do something when editing begin
            } onCommit: {
                self.weatherVM.fetchWeather(cityName: city)
            }
            .padding()
            .background(Color.gray)
            .cornerRadius(10)
            
            Spacer()
            
            Text("Temp \(self.weatherVM.tempreture)")
                .padding()
                .onAppear(){
                    self.weatherVM.fetchWeather(cityName: city)
                }
            Spacer()
            Text(weatherVM.errorMessage)
        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
