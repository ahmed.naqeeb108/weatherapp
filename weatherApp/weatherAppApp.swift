
import SwiftUI

@main
struct weatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
