
import Foundation
struct weatherResponce : Decodable {
    let main : Weather
}

struct Weather : Decodable {
    let temp : Double
    let humidity : Double
}
