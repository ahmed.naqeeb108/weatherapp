
import Foundation

extension URL {
    
    static func urlForWeather(city:String)->URL?{
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=afc73a59f0b8c1bf42e5cc947c215d6e") else {
            return nil
        }
        
        return url
    }
}
