
import Foundation

enum netWorkError : Error{
    case badURL
    case noData
    case deCodingError
}


class weatherService {
    //fetch weather Data
    func getWeather(cityName : String, completion:@escaping (Result<Weather?, netWorkError>)  -> Void) {
        guard let url = URL.urlForWeather(city: cityName) else {
            return completion(.failure(.badURL))
        }
        
        URLSession.shared.dataTask(with: url) { data, responce, error  in
            guard let data = data , error == nil else{
                return completion(.failure(.noData))
            }
            let responce = try? JSONDecoder().decode(weatherResponce.self, from: data)
            if let hasResponce = responce{
                completion(.success(hasResponce.main))
            }else{
                completion(.failure(.deCodingError))
            }

        }.resume()
    }
}
